# deb2squshfs
This bash script converts a .deb packet into a .tcz extension (for TinyCore linux).  
**Don't use it. It will break something.**  
It also creates an md5 checkdumfile, it is __not__ able to create a dependencies file for you.  

The first argument should be the output path, each following argument can be a .deb file.
