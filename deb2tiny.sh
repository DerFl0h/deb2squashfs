#!/bin/bash

#####################################
#DO NOT USE Might be broken
#####################################

# .deb to .tcz converter
# First argument should be the Outputpath , later the input .deb files
path=$(pwd)
i="1"
d=$#
o=$1
echo "Working on "$d" Files" 
echo "I'm in" $path
echo "Outputting to" $o

while [ $i -lt $d ]
do
	echo "Working on: "$2
	echo "Making folder in /tmp"
	mkdir /tmp/$2
	if [ $? -ne 0 ];
	then
		echo "Can not make dir"
		exit 1
	fi
	cp $2 /tmp/$2/
	if [ $? -ne 0 ];
	then
		echo "Can not copy data"
		exit 1
	fi
	cd /tmp/$2
	if [ $? -ne 0 ];
	then
		echo "Can not change dir"
		exit 1
	fi
	echo "Extracting"
	ar x $2 data.tar.xz
	if [ $? -ne 0 ];
	then
		echo "Wrong formate"
		exit 1
	fi
	mkdir data
	mv data.tar.xz data
	cd data
	tar -xf data.tar.xz
	cd $o
	mksquashfs /tmp/$2/data/ $2.tcz
	rm -rf /tmp/$2
	md5sum	$2.tcz > $2.tcz.md5.txt
	shift
	((i++))
	cd $path
done

